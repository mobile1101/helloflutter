import 'package:flutter/material.dart';

enum APP_THEME{LIGHT, DARK}

void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme{

  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            iconTheme: IconThemeData(
              color: Colors.black,
            )
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        )
    );
  }

  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            iconTheme: IconThemeData(
              color: Colors.black,
            )
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade100,
        )
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK ? MyAppTheme.appThemeLight() : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: (){

            setState(() {
              currentTheme == APP_THEME.DARK ? currentTheme = APP_THEME.LIGHT : currentTheme = APP_THEME.DARK;
            });

          },
        ),

      ),
    );
  }
}


  Widget buildCallButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.call,
          ),
          onPressed: () {},
        ),
        Text("Call"),
      ],
    );
  }
  Widget buildTextButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.textsms_outlined,
          ),
          onPressed: () {},
        ),
        Text("Text"),
      ],
    );
  }

  Widget buildVideoCallButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.video_camera_front,
          ),
          onPressed: () {},
        ),
        Text("Video"),
      ],
    );
  }

  Widget buildEmailButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.email_outlined,

          ),
          onPressed: () {},
        ),
        Text("Email"),
      ],
    );
  }

  Widget buildDirectionsButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.directions,

          ),
          onPressed: () {},
        ),
        Text("Directions"),
      ],
    );
  }

  Widget buildPayButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.attach_money,

          ),
          onPressed: () {},
        ),
        Text("Pay"),
      ],
    );
  }


//ListTile

Widget mobliePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),

      onPressed: (){},
    ),

  );
}

Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),

      onPressed: (){},
    ),

  );
}

Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("ITE4T@gmail.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("234 Sunset St,Burlingame"),
    subtitle: Text("home"),    trailing: IconButton(
    icon: Icon(Icons.directions),

    onPressed: (){},
  ),
  );
}

AppBar buildAppBarWidget(){
  return AppBar (
    backgroundColor: Colors.cyan[500],
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    // title: Text("Hello Flutter App"),
    actions: <Widget>[
      IconButton(
          onPressed: (){},
          icon: Icon(
            Icons.star_border_outlined,
            color: Colors.black,
          )
      ),
    ],
  );
}

Widget buildBodyWidget(){
  return ListView (
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            // color: Colors.cyan,
            width: double.infinity,
            //Height constraint at Container widget level
            height: 250,
            child: Image.network(
              "https://lh3.googleusercontent.com/a/AEdFTp5KVsyGMlx92TUc_IM0FHea-l6ESGz_sgBCKe3Vkg=s360-p-rw-no",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(padding: EdgeInsets.all(8.0),
                  child: Text("ITE4T",
                    style: TextStyle(fontSize: 30),
                  ),
                )
              ],
            ),
          ),
          Divider(
            color: Colors.blueGrey[600],
          ),

          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.pink,
                )
              ),
            child: profileActionItems(),
            ),
          ),mobliePhoneListTile(),
          otherPhoneListTile(),
          emailListTile(),
          addressListTile(),
        ],
      ),
    ],
  );
}

Widget profileActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}








